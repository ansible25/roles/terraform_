# terraform_

## Table Of Contents

* [About](#about)
* [Role Defaults](#role-defaults)
* [License](#license)
* [Author](#author)

## About

> Role that installs Terraform tools

[Back to table of contents](#table-of-contents)

## Role Defaults

**Quicklist**: [terraform_docs_digest](#terraform_docs_digest),
[terraform_docs_version](#terraform_docs_version), [tf_digest](#tf_digest),
[tf_version](#tf_version), [tflint_digest](#tflint_digest),
[tflint_version](#tflint_version)

### terraform_docs_digest

* *help*: Expected sha256sum of terraform-docs binary
* *default*: ``37fa36d8340ceebf54f9eda73570ddbccb04fd0a53c133d3deae279161d941a1``

[Back to table of contents](#table-of-contents)

### terraform_docs_version

* *help*: Desired version of terraform-docs
* *default*: ``v0.10.1``

[Back to table of contents](#table-of-contents)

### tf_digest

* *help*: Expected sha256sum of terraform zip
* *default*: ``35c662be9d32d38815cde5fa4c9fa61a3b7f39952ecd50ebf92fd1b2ddd6109b``

[Back to table of contents](#table-of-contents)

### tf_version

* *help*: Desired version of terraform
* *default*: ``0.13.3``

[Back to table of contents](#table-of-contents)

### tflint_digest

* *help*: Expected sha256sum of tflint zip
* *default*: ``0738d0ed41b6f564a78c884a3a0523cbfc3469d0088a6766da362946d3760575``

[Back to table of contents](#table-of-contents)

### tflint_version

* *help*: Desired version of tflint
* *default*: ``v0.20.3``

[Back to table of contents](#table-of-contents)

## License

license (MIT)

[Back to table of contents](#table-of-contents)

## Author

[Brad Stinson](https://www.bradthebuilder.me/)

[Back to table of contents](#table-of-contents)
